FROM amd64/ubuntu:latest

RUN apt update && apt install -y ruby-dev git make build-essential && git clone https://github.com/iagox86/dnscat2.git

RUN cd /dnscat2/server && gem install bundler && bundle install

RUN cd /dnscat2/client && make

CMD ["ruby", "/dnscat2/server/dnscat2.rb", "example.com", "--security", "open", "-a", "ls -lah"]

